// Submit Form
function sendInterest() {
    if (document.getElementById("age").checked == false)
    {
        alert('You must be 21 or older to join.');
        return;
    };

    if ((document.getElementById("state").value == "")
        || (document.getElementById("email").value == "")
        || (document.getElementById("name").value == "")
        || (document.getElementById("lastname").value == "")
        || (document.getElementById("email").value.includes("@") == false)
        || (document.getElementById("email").value.includes(".") == false))
    {
        alert('You need a first and last name, valid email, and home state.');
        return;
    };

    queryParams = '?' + 'email=' + document.getElementById("email").value +
    '&name_first=' + document.getElementById("name").value +
    '&name_last=' + document.getElementById("lastname").value +
    '&home_state=' + document.getElementById("state").value +
    '&nbtv_channel=Spirits&nbtv_interest=Member';

    URL = "https://6bne6d8azh.execute-api.us-east-1.amazonaws.com/default/spiritsPreviewMarketing";
    URL = URL + queryParams;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Request finished. Do processing here.
        }
     };
    xmlhttp.open("GET", URL, true);
    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xmlhttp.send();
    document.getElementById("subscribe").value = "Thanks for your Interest!";
    document.getElementById("subscribe").disabled = true;
};

function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};


function toggleNav() {

    var touchStyle = (isMobileDevice()) ? 'touchstart' : 'click';

    $(".mobile-navigation__close").on(touchStyle, function() {
        $(this).toggleClass("toggled");
        $(".mobile-navigation").find("ul").toggleClass("toggled");
    });
}

toggleNav();


function mobileToC () {
    $('.footer .toc').html('T&C');
}

mobileToC();
